#ifndef LFA_CHOMSKY_H
#define LFA_CHOMSKY_H

/**
 * \file parser.h
 * \brief Arquivo contendo a interface para o verificar se as regras estão na FNC.
 *
 * Arquivo que contém uma função utilizada verificar se o conjunto de regras,
 * que obtidos pelo parser, estão na forma normal de Chomsky.
 *
 * \author Matheus Henrique Nogueira de Paula
 * \author Rhyad Kryn Shamaon Janse
 * \author Victor Grudtner Boell
 */

#include <util/types.h>

/**
 * Checa se uma regra esta na forma normal de chomsky
 * \param variavel nome da regra.
 * \param sentenca sentenca para ser verificada.
 * \return TRUE ou FALSE.
*/
bool_t chomsky(const char variavel, const char* sentenca);

#endif
