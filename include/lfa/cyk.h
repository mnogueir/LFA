#ifndef LFA_CYK_H
#define LFA_CYK_H

/**
 * \file cyk.h
 * \brief Arquivo contendo a interface para o executar o algoritmo CYK.
 *
 * Arquivo que contém uma função utilizada para gerar uma matriz triangular
 * de derivação a partir de um conjunto de regras e uma palavra.
 *
 * \author Matheus Henrique Nogueira de Paula
 * \author Rhyad Kryn Shamaon Janse
 * \author Victor Grudtner Boell
 */

#include <lfa/regra.h>
#include <util/types.h>

/**
 * Executa o algoritmo CYK em um conjunto de regras de uma gramática livre de contexto.
 * \param regras conjunto de regras da gramática.
 * \param palavra palavra que será checada no algoritmo.
 * \param niveis lista encadeada que será populada pela pirâmide gerada pelo algoritmo CYK.
 * \return resultado do algoritmo (verdadeiro se a palavra pertence a linguagem).
 */
bool_t cyk(struct regras_container* regras, const char* palavra, struct lista_encadeada* niveis);

#endif
