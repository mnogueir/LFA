#ifndef LFA_OUTPUT_H
#define LFA_OUTPUT_H

/**
 * \file output.h
 * \brief Arquivo contendo a interface para o salvar o resultado do CYK.
 *
 * Arquivo que contém uma função utilizada para salvar a matriz triangular
 * em um arquivo escolhido pelo usuário.
 *
 * \author Matheus Henrique Nogueira de Paula
 * \author Rhyad Kryn Shamaon Janse
 * \author Victor Grudtner Boell
 */

#include <util/lista.h>

/**
 * Grava em arquivo o resultado do algoritmo CYK.
 * \param camadas lista de camadas da pirâmide gerada pelo algoritmo CYK.
 * \param arquivo nome do arquivo que será gravado.
 */
void gravar_cyk(struct lista_encadeada* camadas, const char* arquivo);

#endif
