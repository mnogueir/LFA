#ifndef LFA_PARSER_H
#define LFA_PARSER_H

/**
 * \file parser.h
 * \brief Arquivo contendo a interface para o parser de regras
 *
 * Arquivo que contém uma função utilizada para interpretar um arquivo
 * e criar um conjunto de regras baseado no conteúdo desse tal arquivo.
 *
 * \author Matheus Henrique Nogueira de Paula
 * \author Rhyad Kryn Shamaon Janse
 * \author Victor Grudtner Boell
 */

#include <lfa/regra.h>
#include <util/types.h>

/**
 * Função que abre um arquivo e usa um parser para interpretá-lo.
 * \param arquivo nome do arquivo de entrada.
 * \param regras ponteiro para o conjunto de regras da gramática.
 */
bool_t parse(const char* arquivo, struct regras_container* regras);


#endif
