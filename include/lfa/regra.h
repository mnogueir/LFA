#ifndef LFA_REGRA_H
#define LFA_REGRA_H

/**
 * \file regra.h
 * \brief Arquivo contendo o tipo Regra
 *
 * Estrutura que armazena as informações de cada regra, permitindo
 * a sua manipulação de forma mais facilitada.
 *
 * \author Matheus Henrique Nogueira de Paula
 * \author Rhyad Kryn Shamaon Janse
 * \author Victor Grudtner Boell
 */

/**
 * \struct regra
 * \brief estrutura que representa uma regra.
 *
 * Uma regra é definida por um nome composto por uma letra maiuscula, e
 * uma lista de sentenças compostas por, ou 2 variaveis ou 1 terminal.
 */
struct regra {
        char nome; /**< Nome da regra */
        struct lista_encadeada* sentencas;      /**< Sentenças da regra */
};

/**
 * Estrutura que armazena todas as regras de uma gramática.
 */
struct regras_container {
        struct lista_encadeada* regras; /**< Lista de regras */
};

/**
 * Cria uma estrutura regras_container vazia.
 * \return ponteiro para a regras_container criada.
 */
struct regras_container* criar_regras();

/**
 * Destroi a estrutura regras_container.
 * \param regras regras que serão destruidas.
 */
void destruir_regras(struct regras_container* regras);

/**
 * Procura uma regra no conjunto de regras baseado em seu nome.
 * \param regras conjunto de regras.
 * \param regra nome da regra que está sendo procurada.
 * \return ponteiro para a regra.
 */
struct regra* buscar_regra_por_nome(struct regras_container* regras, const char regra);

/**
 * Procura uma regra ou mais regras que contenham a sentença especificada.
 * \param regras conjunto de regras.
 * \param sentenca sentença que será usada para localizar as regras.
 * \return lista encadeada com todas as regras que contém a senteça especificada.
 */
struct lista_encadeada* buscar_regras_por_sentenca(struct regras_container* regras, const char* sentenca);

/**
 * Cria uma nova regra.
 * \return ponteiro para a nova regra.
 */
struct regra* criar_regra();

/**
 * Destroi uma regra.
 * \param regra regra a ser destruida.
 */
void destruir_regra(struct regra* regra);

#endif
