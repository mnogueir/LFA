Manual de utilização do programa:

i) Compilação:
        Para compilar o projeto, utilize o programa make para que o projeto possa ser construido
de maneira automática. Para isso, você deve executar o seguinte comando no terminal:

<code>
        make
</code>

ii) Execução
        Para executar o programa, você deve executar o arquivo cyk, encontrado na pasta
build do projeto. O programa recebe 3 parâmetros:

        - entrada: arquivo de entrada da gramática a ser derivada;
        - palavra: palavra que será derivada usando a gramática especificada em "entrada";
        - saida: arquivo onde a saída do algoritmo será gravada.

Para executá-lo, utilize o seguinte comando:

<code>
        ./build/cyk <arquivo_entrada> <palavra> <arquivo_saida>
</code>

iii) Documentação
        O programa conta com uma documentação gerada automáticamente pelo programa Doxygen. 
Para gerá-la, você deve garantir que o programa Doxygen esteja instalado e deve executar o seguinte
comando no terminal:

<code>
        make documentation
</code>

Isso irá criar um diretório docs, onde duas outras pastas serão criadas: html e latex. Dentro da pasta
html existem diversos arquivos html com a documentação do projeto. Para visualizar a documentação em HTML,
abra o arquivo index.html. Existe também a possibilidade de visualizar a documentação em formado PDF. Esse
arquivo está localizado em docs/latex/refman.pdf.

iv) Apagar os arquivos objeto
        Para limpar o projeto, você pode utilizar o programa make. Para fazer isso, digite o seguinte
comando no terminal:

<code>
        make clean
</code>

v) Arquivos de teste
        Os arquivos de teste estão especificados na pasta entrada do projeto.
