#include <lfa/chomsky.h>
#include <util/string.h>
#include <string.h>

/* Verifica se uma determinada regra está de acordo com a forma normal de chomsky. */
bool_t chomsky(const char variavel, const char* sentenca)
{
        /* Detecta loop no simbolo inicial */
        if (strlen(sentenca) == 2 && variavel == 'S' &&
                (sentenca[0] == 'S' || sentenca[1] == 'S'))
                return FALSE;
        /* Checa se a sentença é um terminal */
        if (strlen(sentenca) == 1 && char_inrange(sentenca[0], 'a', 'z'))
                return TRUE;
        /* Checa se a sentença é composta por 2 variaveis. */
        if (strlen(sentenca) == 2 && char_inrange(sentenca[0], 'A', 'Z') &&
                char_inrange(sentenca[1], 'A', 'Z'))
                return TRUE;
        /* Checa se só o simbolo inicial possui lambda */
        if (strlen(sentenca) == 1 && sentenca[0] == '.' && variavel == 'S')
                return TRUE;
        return FALSE;
}
