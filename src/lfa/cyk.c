#include <lfa/cyk.h>
#include <lfa/regra.h>
#include <util/lista.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Função que pega um elemento dentro de uma lista de lista. Mesmo que pegar um elemento de
 * uma matriz:
 *
 * lista[x][y].
 */
void* pegar_item_por_indice(struct lista_encadeada* niveis, unsigned int x, unsigned int y)
{
        struct lista_encadeada* lista = (struct lista_encadeada*) 
                pegar_elemento_lista(niveis, y);
        return pegar_elemento_lista(lista, x);
}

/* Cria uma lista de permutações entre dois conjuntos de sentenças:
 * exemplo:
 *
 * AB e T ==> AT, BT
 */
struct lista_encadeada* permuta(const char* a, const char* b)
{
        unsigned i, j;
        char* buffer;
        struct lista_encadeada* lista = criar_lista();
        for (i = 0; i < strlen(a); ++i) {
                for (j = 0; j < strlen(b); ++j) {
                        buffer = (char*) malloc(sizeof(char) * 3);
                        buffer[0] = '\0';
                        strncpy(buffer, a+i, 1);
                        buffer[1] = '\0';
                        strncat(buffer, b+j, 1);
                        buffer[2] = '\0';
                        adicionar_elemento_lista(lista, buffer);
                }
        }
        return lista; 
}

/* Função que permite navegar nas listas que representam a matriz triangular do algorimto
 * CYK. Essa função navega na matriz utilizando as coordenadas especificadas na função cyk, a
 * qual utiliza de uma formula matemática para encontrar todas as possíveis combinações de
 * pontos que podem ser permutados.
 *
 * Seja P o ponto que está sendo analisado, e P0 e P1 os dois pontos que serão analisados para
 * descobrir derivações das combinações entre seus respectivos conteúdos, com isso, gerando
 * o conteúdo de P.
 *
 * Ao navegar na matriz, essa função irá procurar todas as permutações entre as variaveis
 * encontradas em P0 e P1. Caso encontre alguma regra que gere uma dessas permutações, adiciona
 * a regra ao ponto P que está sendo analisado.
 */
bool_t navegar_piramide(struct regras_container* regras,
                        struct lista_encadeada* niveis,
                        char* buff,
                        unsigned int x0,
                        unsigned int y0,
                        unsigned int x1,
                        unsigned int y1)
{
        unsigned int i, j, k, tam;
        char* r1 = (char*) pegar_item_por_indice(niveis, x0, y0);
        char* r2 = (char*) pegar_item_por_indice(niveis, x1, y1);
        unsigned int tamanho = strlen(r1) + strlen(r2);
        char* permutacao;
        char* aux;
        struct lista_encadeada *r, *permutacoes;
        tam = 0;
        permutacoes = permuta(r1, r2);
        for (i = 0; i < permutacoes->tamanho; ++i) {
                permutacao = (char*) pegar_elemento_lista(permutacoes, i);
                r = buscar_regras_por_sentenca(regras, permutacao);
                tam += r->tamanho;
                for (k = 0; k < r->tamanho; ++k) {
                        aux = (char*) pegar_elemento_lista(r, k);
                        if (str_contains_str(aux, buff) == FALSE)
                                strcat(buff, aux);
                        /*buff[strlen(aux) + strlen(buff)] = '\0';*/
                }

        }
        if (tam == 0)
                return FALSE;
        return TRUE;
}

/* Função que coloca a palavra na primeira camada da matriz, e a partir dessa camada, gera a segunda
 * camada. A partir da segunda camada, a função navegar_piramide consegue completar a matriz utilizando
 * a formula descrita acima.
 */
void gerar_primeiras_camadas(struct lista_encadeada *niveis, const char* palavra, struct regras_container* regras)
{
        struct lista_encadeada* nivel_palavra = criar_lista();
        struct lista_encadeada* nivel_atual = criar_lista();
        struct lista_encadeada* aux;
        unsigned int tamanho = strlen(palavra);
        unsigned int i, j;
        char* buffer;
        char aux_buff[1];
        /* Adiciona o primeiro e segundo nivel da palavra */
        adicionar_elemento_lista(niveis, nivel_palavra);
        adicionar_elemento_lista(niveis, nivel_atual);
        /* Preenche o primeiro nível da pirâmide com a palavra */
        for (i = 0; i < tamanho; ++i) {
                buffer = (char*) malloc(sizeof(char) * 2);
                strncpy(buffer, palavra+i, 1);
                buffer[1] = '\0';
                /* Adiciona cada letra da palavra numa posição do nível 0 da pirâmide. */
                adicionar_elemento_lista(nivel_palavra, buffer);
        }
        /* Popula o nível 1 da pirâmide */
        for (i = 0; i < tamanho; ++i) {
                buffer = (char*) pegar_elemento_lista(nivel_palavra, i);
                aux = buscar_regras_por_sentenca(regras, buffer);
                buffer = (char*) malloc(sizeof(char) * aux->tamanho + 1);
                buffer[0] = '\0';
                for (j = 0; j < aux->tamanho; ++j) {
                        aux_buff[0] = *((char*) pegar_elemento_lista(aux, j));
                        aux_buff[1] = '\0';
                        strcat(buffer, aux_buff);
                }
                adicionar_elemento_lista(nivel_atual, buffer);
        }
}

/* Gera a matriz do CYK. Nessa função são geradas todas as camadas da matriz a partir da
 * palavra de entrada do algoritmo.
 *
 * Nessa função, os indices da matriz são percorridos utilizando as seguintes formulas:
 *
 * P  = (x, y)
 * P0 = (x, y-1)
 * P1 = (x + l, y - l), sendo l o número da linha da matriz.
 * P0' = (P0.x, P0.y - 1)
 * P1' = (P1.x - 1, P1.y + 1)
 */
bool_t cyk(struct regras_container* regras, const char* palavra, struct lista_encadeada* niveis)
{
        unsigned int i, j;
        unsigned int tamanho = strlen(palavra);
        unsigned int camada = 0;
        bool_t verificador = FALSE;
        unsigned int x0, x1, y0, y1;
        struct lista_encadeada* camada_aux;
        char* buffer;
        /* Cria as duas primeiras camadas da pirâmide */
        gerar_primeiras_camadas(niveis, palavra, regras);
        /* Cria as camadas 2 até a camada N, sendo N = tam(palavra) */
        camada = 2;
        while (camada <= tamanho) {
                camada_aux = criar_lista();
                for (j = 0; j < tamanho - camada + 1; j++) {
                        buffer = (char*) malloc(sizeof(char) * 50);
                        buffer[0] = '\0';
                        x0 = j;
                        y0 = camada-1;
                        x1 = x0 + (camada - 1);
                        y1 = y0 - (camada - 2);
                        verificador += navegar_piramide(regras, niveis, buffer, x0, y0, x1, y1);
                        for (i = 0; i < camada-2; ++i) {
                                --y0;
                                --x1;
                                ++y1;
                                verificador += navegar_piramide(regras, niveis, buffer, x0, y0, x1, y1);
                        }
                        if (verificador == FALSE) {
                                adicionar_elemento_lista(camada_aux, "-");
                        } else {
                                adicionar_elemento_lista(camada_aux, buffer);
                        }
                        verificador = FALSE;
                }
                adicionar_elemento_lista(niveis, camada_aux);
                ++camada;
        }
        return TRUE;
}
