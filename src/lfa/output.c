#include <lfa/output.h>
#include <stdio.h>
#include <string.h>

/*
 * Padroniza a saída do algoritmo CYK para que o arquivo de saída tenha o formato
 * especificado pela descrição do trabalho.
 */
void padronizar_saida_cyk(const char* entrada, char* saida)
{
        unsigned int i = 0;
        unsigned int j = 1;
        unsigned int tamanho = strlen(entrada);
        saida[0] = '{';
        for (; i < tamanho; ++i) {
                if (entrada[i] != '-') {
                        saida[j++] = entrada[i];
                }
                saida[j++] = ',';
        }
        saida[j-1] = '}';
        saida[j] = '\0';
}

/*
 * Grava a matriz do algoritmo CYK em um arquivo de saída especificado.
 */
void gravar_cyk(struct lista_encadeada* camadas, const char* arquivo)
{
        unsigned int i;
        unsigned int j;
        unsigned int nivel;
        struct lista_encadeada* aux;
        char *entrada;
        char buffer[50];
        /* Abre o arquivo */
        FILE* f = fopen(arquivo, "w");
        if (f == NULL) {
                printf("O arquivo %s nao pode ser criado \n.", arquivo);
                return;
        }
        for (i = 0; i < camadas->tamanho; ++i) {
                nivel = camadas->tamanho - i - 1;
                aux = (struct lista_encadeada*) pegar_elemento_lista(camadas, nivel);
                if (nivel)
                        fprintf(f, "%d\t", nivel);
                else
                        fprintf(f, "p\t");
                for (j = 0; j < aux->tamanho; ++j) {
                        entrada = (char*) pegar_elemento_lista(aux, j);
                        padronizar_saida_cyk(entrada, buffer);
                        fprintf(f, "%s\t", buffer);
                }
                fprintf(f, "\n");
        }
        fclose(f);
}
