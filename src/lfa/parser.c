#include <lfa/parser.h>
#include <lfa/regra.h>
#include <util/lista.h>
#include <util/string.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/**
 * Parseia uma regra e extrai o seu nome e suas sentenças.
 * \param regra linha de texto que define a regra.
 * \param regras container de regras que armazena todas as regras da gramática.
 * \return status da operação.
 */
bool_t parse_regra(const char* regra, struct regras_container* regras);

/**
 * Checa se todas as sentenças das regras contém somente variáveis que existem na gramática.
 * \param regras container de regras da gramática.
 * \return status da operação.
 */
bool_t checar_variavel_sentenca(struct regras_container* regras);

/* Abre o arquivo de entrada especificado, e usa um outro parser para cada linha do
 * arquivo. Toda regra extraída será gravada na estrutura "regras".
 */
bool_t parse(const char* arquivo, struct regras_container* regras)
{
        FILE* f = fopen(arquivo, "r");
        char *line = NULL;
        size_t len = 0;
        if (f == NULL) {
                printf("Erro: Arquivo de entrada não existe!\n");
                return FALSE;
        }
        while (getline(&line, &len, f) != -1) {
                if (parse_regra(line, regras) == FALSE) {
                        fclose(f);
                        return FALSE;
                }
        }
        fclose(f);

        return checar_variavel_sentenca(regras);
}

/* Função que recebe uma linha do arquivo de entrada e tenta extrair uma regra e suas
 * sentenças. Essa função realiza algumas checagens de formato do arquivo, por exemplo:
 *
 * Toda linha deve conter uma variável [A-Z]->(algum conteudo). 
 *
 * Caso algum problema seja encontrado, a função parará a sua execução e mostrará
 * um erro ao usuário. Essa função também faz checagens para verificar se a regra
 * extraída está na forma normal de chomsky.
 */
bool_t parse_regra(const char* regra, struct regras_container* regras)
{
        /* Flag que indica se a regra já foi adicionada previamente */
        bool_t repetida = TRUE;
        /* Remove os espaços, tabs e quebra de linhas da regra */
        char* r = str_trim(regra);
        r = str_remove_tab(r);
        r = str_sanitize(r);
        /*
         * Cada regra é definida como:
         * [A-Z]->[A-Z](2)|.| ou
         * [A-Z]->[a-z]
         */
        char seta[2];
        char variavel = r[0];
        char *buffer;
        struct regra* sentenca;
        /* Checa se a variavel não é uma letra minuscula. */
        if (!char_inrange(variavel, 'A', 'Z')) {
                printf("Erro: Variavel deve ser maiuscula.\n");
                return FALSE;
        }
        strncpy(seta, (r+1), 2);
        seta[2] = '\0';
        /* Checa se depois da variavel, contém uma seta (->). */
        if (strcmp(seta, "->")) {
                printf("Erro: Seta (->) deve ser colocada após a variavel da regra.\n");
                return FALSE;
        }
        /* Procura a regra no conjunto de regras, se ela não existir, cria uma nova */
        sentenca = buscar_regra_por_nome(regras, variavel);
        if (sentenca == NULL) {
                repetida = FALSE;
                sentenca = criar_regra();
                sentenca->nome = variavel;
        }
        /* Pega as sentenças da regra */
        buffer = strtok(r+3, "|");
        while (buffer != NULL) {
                /* Verifica se a sentença está na FNC */
                if (chomsky(variavel, buffer) == FALSE) {
                        printf("Regra: %s nao esta na forma normal de chomsky\n", regra);
                        return FALSE;
                }
                adicionar_elemento_lista(sentenca->sentencas, buffer);
                buffer = strtok(NULL, "|");
        }
        if (repetida == FALSE)
                adicionar_elemento_lista(regras->regras, sentenca);
        return TRUE;
}

/* Checa se todas as variáveis em uma sentença contém uma regra na gramática.
 */
bool_t checar_variavel_sentenca(struct regras_container* regras)
{
        /* Checa se todas as variaveis das sentenças possuem uma regra. */
        int i, j;
        char* sentenca;
        struct regra* regra;
        for (i = 0; i < regras->regras->tamanho; ++i) {
                regra = (struct regra*) pegar_elemento_lista(regras->regras, i);
                for (j = 0; j < regra->sentencas->tamanho; ++j) {
                        sentenca = pegar_elemento_lista(regra->sentencas, j);
                        if (strlen(sentenca) == 2 && 
                                (!buscar_regra_por_nome(regras, sentenca[0]) ||
                                !buscar_regra_por_nome(regras, sentenca[1]))) {
                                        printf("Erro: Sentenca %s na regra %c contem uma variavel que nao existe!\n",
                                        sentenca, regra->nome);
                                        return FALSE;
                                }
                }
        }
        return TRUE;
}
