#include <lfa/regra.h>
#include <util/lista.h>
#include <string.h>
#include <stdlib.h>

/* Equivalente ao strcmp do C, porém, utilizando duas regras como entrada. */
int compara_nome_regra(void* a, void* b)
{
        struct regra* r1 = (struct regra*) a;
        struct regra* r2 = (struct regra*) b;
        return r1->nome - r2->nome;
}

/* Cria uma nova estrutura para armazenar as regras do programa. */
struct regras_container* criar_regras()
{
        struct regras_container* regras = (struct regras_container*)
                malloc(sizeof(struct regras_container));
        regras->regras = criar_lista();
}

/* Destroi o conjunto de regras criado pelo criar_regras. */
void destruir_regras(struct regras_container* regras)
{
        destruir_lista(regras->regras);
        free(regras);
}

/* Procura uma regra baseado no nome dessa regra, caso encontre, retorna a referencia para
 * a estrutura que está armazenando tal regra, caso não seja encontrada, a função retorna NULL.
 */
struct regra* buscar_regra_por_nome(struct regras_container* regras, const char regra)
{
        procurar_lista(regras->regras, ((void*) &regra), compara_nome_regra);
}

/* Procura uma regra baseado em uma variável ou terminal que esteja na sentença de alguma regra. Caso
 * encontre, retorna todas as regras que contém tal variável ou terminal em sua sentença.
 */
struct lista_encadeada* buscar_regras_por_sentenca(struct regras_container* regras, const char* sentenca)
{
        int i, j;
        struct lista_encadeada* lista = criar_lista();
        struct regra* regra;
        char* buffer;
        /* Percorre todas as regras comparando suas sentenças com a sentença procurada. */
        for (i = 0; i < regras->regras->tamanho; ++i) {
                regra = (struct regra*) pegar_elemento_lista(regras->regras, i);
                /* Percorre todas as senteças da regra. */
                for (j = 0; j < regra->sentencas->tamanho; ++j) {
                        buffer = (char*) pegar_elemento_lista(regra->sentencas, j);
                        if (strcmp(buffer, sentenca) == 0) {
                                adicionar_elemento_lista(lista, &(regra->nome));
                                break;
                        }
                }
        }
        return lista;
}

/* Cria uma nova regra. */
struct regra* criar_regra()
{
        struct regra* regra = (struct regra*) malloc(sizeof(struct regra));
        regra->sentencas = criar_lista();
        return regra;
}

/* Destroi uma regra */
void destruir_regra(struct regra* regra)
{
        destruir_lista(regra->sentencas);
        free(regra);
}
