#include <lfa/cyk.h>
#include <lfa/parser.h>
#include <util/lista.h>
#include <stdio.h>

int main(int argc, char** argv)
{
        if (argc < 4) {
                printf("Erro de sintaxe!\n");
                printf("Sintaxe: %s <arquivo_entrada> <palavra> <arquivo_saida>\n", argv[0]);
                return 1;
        }
        struct regras_container *container = criar_regras();
        struct lista_encadeada *piramide_cyk = criar_lista();
        parse(argv[1], container);
        cyk(container, argv[2], piramide_cyk);
        gravar_cyk(piramide_cyk, argv[3]);
        return 0;
}
